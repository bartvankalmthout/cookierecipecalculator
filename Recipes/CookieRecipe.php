<?php


namespace Recipes;


use Recipes\Helpers\IngredientList;
use Recipes\Helpers\IngredientListItem;
use Recipes\Helpers\RecipeResult;

class CookieRecipe implements RecipeInterface
{
    /**
     * int
     */
    const MAXIMUMAMOUTTEASPOONS = 100;

    /**
     * @var IngredientList
     */
    private $ingredientsList;

    /**
     * @var RecipeResult
     */
    private $recipeResult;

    public function setIngredientsList(IngredientList $ingredientsList)
    {
        $this->ingredientsList = $ingredientsList;
    }

    public function cookRecipe(): RecipeResult
    {
        $this->recipeResult = new RecipeResult();

        /** @var IngredientListItem $ingredientListItem */
        foreach ($this->ingredientsList->getIngredientListItems() as $ingredientListItem) {

            $ingredientObject = $ingredientListItem->getIngredient();
            $amount = $ingredientListItem->getAmount();

            $this->recipeResult->addToCapacity($ingredientObject->getCapacity() * $amount);
            $this->recipeResult->addToDurability($ingredientObject->getDurability() * $amount);
            $this->recipeResult->addToFlavor($ingredientObject->getFlavor() * $amount);
            $this->recipeResult->addToTexture($ingredientObject->GetTexture() * $amount);
            $this->recipeResult->calculateTotal();
        }

        return $this->recipeResult;
    }

    public function getRecipeResult(): RecipeResult
    {
        return $this->recipeResult;
    }

    public function getMaximumAmountTeaSpoons(): int
    {
        return self::MAXIMUMAMOUTTEASPOONS;
    }

    public function getIngredientsList(): IngredientList
    {
        return $this->ingredientsList;
    }



}