<?php


namespace Recipes;


use Recipes\Helpers\IngredientList;
use Recipes\Helpers\RecipeResult;

interface RecipeInterface
{
    public function setIngredientsList(IngredientList $ingredientsList);
    public function cookRecipe(): RecipeResult;
    public function getRecipeResult(): RecipeResult;
    public function getMaximumAmountTeaSpoons(): int;
    public function getIngredientsList(): IngredientList;


}