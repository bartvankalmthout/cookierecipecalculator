<?php


namespace Recipes\Helpers;


use Ingredients\IngredientInterface;

class IngredientListItem
{
    /**
     * @var int
     */
    private $amount;

    /**
     * @var IngredientInterface
     */
    private $ingredient;

    /**
     * IngredientListItem constructor.
     * @param IngredientInterface $ingredient
     * @param int $amount
     */
    public function __construct(IngredientInterface $ingredient, int $amount)
    {
        $this->ingredient = $ingredient;
        $this->amount = $amount;
    }

    /**
     * @return int
     */
    public function getAmount(): int
    {
        return $this->amount;
    }

    /**
     * @return IngredientInterface
     */
    public function getIngredient(): IngredientInterface
    {
        return $this->ingredient;
    }

    /**
     * @param int $value
     */
    public function raiseAmount(int $value) {
        $this->amount += $value;
    }

    /**
     * @param int $value
     */
    public function lowerAmount(int $value) {
        $this->amount -= $value;
        if($this->amount < 0)
            $this->amount = 0;
    }
}