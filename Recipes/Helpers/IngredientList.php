<?php


namespace Recipes\Helpers;


class IngredientList
{
    /**
     * @var array
     */
    private $ingredientListItems;

    /**
     * @param IngredientListItem $ingredientListItem
     */
    public function addIngredientListItem(IngredientListItem $ingredientListItem) {
        $this->ingredientListItems[] = $ingredientListItem;
    }

    



    /**
     * @return array
     */
    public function getIngredientListItems(): array
    {
        return $this->ingredientListItems;
    }

    public function __toString()
    {
        $totalItems = 0;
        $output = '';

        /** @var IngredientListItem $ingredientListItem */
        foreach ($this->getIngredientListItems() as $ingredientListItem) {
            $output .= $ingredientListItem->getIngredient()->getName() . ': ' . $ingredientListItem->getAmount() .  ' ';
            $totalItems += $ingredientListItem->getAmount();
        }

        $output = 'Ingredients (' . $totalItems . '): ' . $output;

        return $output . PHP_EOL;
    }

}