<?php


namespace Recipes\Helpers;


class RecipeResult
{

    /**
     * @var int
     */
    private $totalScore;
    /**
     * @var int
     */
    private $totalCapacity;
    /**
     * @var int
     */
    private $totalDurability;
    /**
     * @var int
     */
    private $totalFlavor;
    /**
     * @var int
     */
    private $totalTexture;

    /**
     * @return int
     */
    public function getTotalScore(): int
    {
        return $this->totalScore;
    }

    /**
     * @return int
     */
    public function getTotalCapacity(): int
    {
        return $this->totalCapacity;
    }

    /**
     * @return int
     */
    public function getTotalDurability(): int
    {
        return $this->totalDurability;
    }

    /**
     * @return int
     */
    public function getTotalFlavor(): int
    {
        return $this->totalFlavor;
    }

    /**
     * @return int
     */
    public function getTotalTexture(): int
    {
        return $this->totalTexture;
    }

    /**
     * @param int $valueToAdd
     */
    public function addToCapacity(int $valueToAdd)
    {
        $this->totalCapacity += $valueToAdd;
    }

    /**
     * @param int $valueToAdd
     */
    public function addToDurability(int $valueToAdd)
    {
        $this->totalDurability += $valueToAdd;
    }

    /**
     * @param int $valueToAdd
     */
    public function addToFlavor(int $valueToAdd)
    {
        $this->totalFlavor += $valueToAdd;
    }

    /**
     * @param int $valueToAdd
     */
    public function addToTexture(int $valueToAdd)
    {
        $this->totalTexture += $valueToAdd;
    }

    /**
     *
     */
    public function calculateTotal()
    {

        $this->totalScore = max($this->totalCapacity, 0) * max($this->totalDurability, 0) * max($this->totalFlavor, 0) * max($this->totalTexture, 0);
    }


    public function __toString()
    {
        return
            'Total score: ' . $this->getTotalScore() . PHP_EOL .
            'Sub scores: capacity:' . $this->getTotalCapacity() . ' ' .
            'durability:' . $this->getTotalDurability() . ' ' .
            'flavor:' . $this->getTotalFlavor() . ' ' .
            'texture:' . $this->getTotalTexture() . PHP_EOL;
    }

}