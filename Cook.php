<?php

use Ingredients\Butterscotsh;
use Ingredients\Candy;
use Ingredients\Chocolate;
use Ingredients\Sprinkles;
use RecipeEnhancementCalculators\CookieRecipeEnhancer;
use Helpers\BestCookie;
use Recipes\CookieRecipe;
use Recipes\Helpers\IngredientList;
use Recipes\Helpers\IngredientListItem;

require_once './vendor/autoload.php';

$highscore = 0;
$bestCookie = '';

$ingredientsList = new IngredientList();
$ingredientsList->addIngredientListItem(new IngredientListItem(new Butterscotsh(), 25));
$ingredientsList->addIngredientListItem(new IngredientListItem(new Chocolate(), 25));
$ingredientsList->addIngredientListItem(new IngredientListItem(new Sprinkles(), 25));
$ingredientsList->addIngredientListItem(new IngredientListItem(new Candy(), 25));

$recipe = new CookieRecipe();
$recipeEnhancementCalculator = new CookieRecipeEnhancer();

$test = 1;
$totalAmount = 1000000000;

$recipe->setIngredientsList($ingredientsList);
$recipeResult = $recipe->cookRecipe();
$bestCookie = new BestCookie($ingredientsList, $recipeResult);

while ($ingredientsList = $recipeEnhancementCalculator->calculateBetterIngredientList()) {

    $recipe->setIngredientsList($ingredientsList);
    $recipeResult = $recipe->cookRecipe();

    if($recipeResult->getTotalScore() > $highscore) {
        $highscore = $recipeResult->getTotalScore();
        $bestCookie = new BestCookie($ingredientsList, $recipeResult);
        echo 'test: ' . $test . PHP_EOL;
        echo $recipe->getIngredientsList();
        echo $recipeResult . ($recipeResult->getTotalScore() >= $highscore ? 'NEW HIGHSCORING RECIPE' . PHP_EOL :'');
    }

    $test++;

    if($test === $totalAmount) {
        echo 'highscore: ' . $highscore . PHP_EOL;
        echo $bestCookie->getIngredientList() . PHP_EOL;
        echo $bestCookie->getRecipeResult() . PHP_EOL;
        die('Dit was het wel zo\'n beetje (statistisch gezien). Shutting down');
    }
}

