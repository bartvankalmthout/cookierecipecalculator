<?php


namespace RecipeEnhancementCalculators;

use Exception;
use Ingredients\Butterscotsh;
use Ingredients\ButterscotshExample;
use Ingredients\Candy;
use Ingredients\Chocolate;
use Ingredients\CinnamonExample;
use Ingredients\IngredientInterface;
use Ingredients\Sprinkles;
use Recipes\CookieRecipe;
use Recipes\Helpers\IngredientList;
use Recipes\Helpers\IngredientListItem;

class ExampleCookieRecipeEnhancer implements RecipeEnhancementCalculatorInterface
{

    /**
     * @return IngredientList
     * @throws Exception
     */
    public function calculateBetterIngredientList(): IngredientList
    {
        $newTest = new IngredientList();

        $teaSpoonsLeft = CookieRecipe::MAXIMUMAMOUTTEASPOONS;

        for($i=0 ; $i<2 ; $i++) {
            $part = random_int(0, $teaSpoonsLeft);
            $teaSpoonsLeft = $teaSpoonsLeft - $part;

            $newTest->addIngredientListItem(new IngredientListItem($this->getRandomIngredient(), $part));
        }

        return $newTest;
    }

    /**
     * @return IngredientInterface
     */
    private function getRandomIngredient() {
        $ingredientsAvailable = [
            ButterscotshExample::class,
            CinnamonExample::class,
        ];

        $key = array_rand($ingredientsAvailable);
        return new $ingredientsAvailable[$key]();
    }
}