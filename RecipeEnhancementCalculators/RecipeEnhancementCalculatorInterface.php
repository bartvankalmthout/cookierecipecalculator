<?php


namespace RecipeEnhancementCalculators;


use Recipes\Helpers\IngredientList;

interface RecipeEnhancementCalculatorInterface
{
    public function calculateBetterIngredientList(): IngredientList;
}