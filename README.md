#Cookie RecipeCalculator
This project will determine the optimal cookie ingredients.

Run the following commands:
```shell script
composer dump-autoload
php ./Cook.php
```

result to be expected:
```shell script
Ingredients (100): Chocolate: 38 Sprinkles: 17 Candy: 26 Butterscotsh: 19 
Total score: 21367368
Sub scores: capacity:34 durability:69 flavor:99 texture:92
NEW HIGHSCORING RECIPE
```

After approximately 25000 iterations the optimum value will be reached.

To test the code and reproduce the example, run:
```shell script
php ./ExampleCook.php
```

result to be expected:
```shell script
Ingredients (100): Cinnamon: 56 Butterscotsh: 44 
Total score: 62842880
Sub scores: capacity:68 durability:80 flavor:152 texture:76
NEW HIGHSCORING RECIPE
```