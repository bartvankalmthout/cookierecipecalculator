<?php


namespace Helpers;

use Recipes\Helpers\IngredientList;
use Recipes\Helpers\RecipeResult;

class BestCookie
{
    /**
     * @var IngredientList
     */
    private $ingredientList;
    /**
     * @var RecipeResult
     */
    private $recipeResult;

    /**
     * BestCookie constructor.
     * @param IngredientList $ingredientList
     * @param RecipeResult $recipeResult
     */
    public function __construct(IngredientList $ingredientList, RecipeResult $recipeResult)
    {
        $this->ingredientList = $ingredientList;
        $this->recipeResult = $recipeResult;
    }

    /**
     * @return IngredientList
     */
    public function getIngredientList(): IngredientList
    {
        return $this->ingredientList;
    }

    /**
     * @return RecipeResult
     */
    public function getRecipeResult(): RecipeResult
    {
        return $this->recipeResult;
    }

}