<?php


namespace Ingredients;


class Candy implements IngredientInterface
{

    /**
     * @return string
     */
    public function getName(): string
    {
        return "Candy";
    }

    /**
     * @return int
     */
    public function getCapacity(): int
    {
        return 0;
    }

    /**
     * @return int
     */
    public function getDurability(): int
    {
        return -1;
    }

    /**
     * @return int
     */
    public function getFlavor(): int
    {
        return 0;
    }

    /**
     * @return int
     */
    public function getTexture(): int
    {
        return 5;
    }
}