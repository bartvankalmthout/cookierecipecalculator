<?php

namespace Ingredients;

interface IngredientInterface
{
    /**
     * @return string
     */
    public function getName(): string;

    /**
     * @return int
     */
    public function getCapacity(): int;

    /**
     * @return int
     */
    public function getDurability(): int;

    /**
     * @return int
     */
    public function getFlavor(): int;

    /**
     * @return int
     */
    public function getTexture(): int;
}