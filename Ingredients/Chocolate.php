<?php


namespace Ingredients;


class Chocolate implements IngredientInterface
{

    /**
     * @return string
     */
    public function getName(): string
    {
        return "Chocolate";
    }

    /**
     * @return int
     */
    public function getCapacity(): int
    {
        return 0;
    }

    /**
     * @return int
     */
    public function getDurability(): int
    {
        return 0;
    }

    /**
     * @return int
     */
    public function getFlavor(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function getTexture(): int
    {
        return -1;
    }
}