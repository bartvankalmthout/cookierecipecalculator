<?php


namespace Ingredients;


class ButterscotshExample implements IngredientInterface
{

    /**
     * @return string
     */
    public function getName(): string
    {
        return "Butterscotsh";
    }

    /**
     * @return int
     */
    public function getCapacity(): int
    {
        return -1;
    }

    /**
     * @return int
     */
    public function getDurability(): int
    {
        return -2;
    }

    /**
     * @return int
     */
    public function getFlavor(): int
    {
        return 6;
    }

    /**
     * @return int
     */
    public function getTexture(): int
    {
        return 3;
    }
}