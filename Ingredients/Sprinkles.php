<?php


namespace Ingredients;


class Sprinkles implements IngredientInterface
{

    /**
     * @return string
     */
    public function getName(): string
    {
        return "Sprinkles";
    }

    /**
     * @return int
     */
    public function getCapacity(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function getDurability(): int
    {
        return 0;
    }

    /**
     * @return int
     */
    public function getFlavor(): int
    {
        return -2;
    }

    /**
     * @return int
     */
    public function getTexture(): int
    {
        return 0;
    }
}