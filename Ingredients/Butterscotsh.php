<?php


namespace Ingredients;


class Butterscotsh implements IngredientInterface
{

    /**
     * @return string
     */
    public function getName(): string
    {
        return "Butterscotsh";
    }

    /**
     * @return int
     */
    public function getCapacity(): int
    {
        return 0;
    }

    /**
     * @return int
     */
    public function getDurability(): int
    {
        return 5;
    }

    /**
     * @return int
     */
    public function getFlavor(): int
    {
        return -3;
    }

    /**
     * @return int
     */
    public function getTexture(): int
    {
        return 0;
    }
}