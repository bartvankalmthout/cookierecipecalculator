<?php


namespace Ingredients;


class CinnamonExample implements IngredientInterface
{

    /**
     * @return string
     */
    public function getName(): string
    {
        return "Cinnamon";
    }

    /**
     * @return int
     */
    public function getCapacity(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function getDurability(): int
    {
        return 3;
    }

    /**
     * @return int
     */
    public function getFlavor(): int
    {
        return -2;
    }

    /**
     * @return int
     */
    public function getTexture(): int
    {
        return -1;
    }
}